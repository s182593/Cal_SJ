package calc_sj;

public class sj_cal {
	
	public static void main(String args[]){
		sj_cal cal = new sj_cal();
	
		System.out.println(cal.Add("//;\n2;3"));

		}
		
		
	public int Add(String num){
					
		int sum = 0;
		String delimiter;
		
			if (noCalculatorInput(num)) {
				return sum;
			
			}else if(oneCalculatorInput(num)){
				
				sum = Integer.parseInt(num);
				
				return sum;

			}
				
			else {
				
				if(customDelimiter(num)){
					
					delimiter = num.substring(2, 3);
					num = num.replace("//"+delimiter+"\n", "");

				}else{
					
					delimiter = ",|\\\n";
				
				}
				
				String[] inputNumber =  num.split(delimiter);

			    int number [] = new int[inputNumber.length];
					     
			    for(int i=0; i<inputNumber.length;i++){
				 
			       number[i]=Integer.parseInt(inputNumber[i]);
			       
			       if(numberIsPositive(number, i)){
			    	   
			    	   throw new IllegalArgumentException("Nagative not allowed");
			    	   
			       }	       
			       
			       sum += number[i];
			 }		

			 return sum;
			 
			}
	}


	private boolean numberIsPositive(int[] number, int i) {
		return number[i]< 0;
	}


	private boolean customDelimiter(String num) {
		return num.substring(0, 2).equals("//");
	}


	private boolean oneCalculatorInput(String num) {
		return num.matches("\\d+");
	}


	private boolean noCalculatorInput(String num) {
		return num.isEmpty();
	}
			
}
		
		

	


