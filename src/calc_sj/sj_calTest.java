package calc_sj;

import static org.junit.Assert.*;

import org.junit.Test;

public class sj_calTest {

	@Test
	public void checkForNoInput() {
		
		
		sj_cal test = new sj_cal();
        
        assertEquals(0,test.Add(""));
		
//		fail("Not yet implemented");
	}
	
	@Test
	public void checkForOneNumber() {
		
		
		sj_cal test = new sj_cal();
        
        assertEquals(10,test.Add("10"));
		
//		fail("Not yet implemented");
	}
	
	@Test
	public void checkForTwoNumbers() {
		
		
		sj_cal test = new sj_cal();
        
        assertEquals(3,test.Add("1,2"));
		
//		fail("Not yet implemented");
	}
	
	@Test
	public void checkForUnkownAmoutOfNumber() {
		
		
		sj_cal test = new sj_cal();
        
        assertEquals(6,test.Add("1,2,3"));
		
//		fail("Not yet implemented");
	}
	
	@Test
	public void checkForNewLines() {
		
		sj_cal test = new sj_cal();
        
        assertEquals(8,test.Add("1\n2,5"));

	}
	
	@Test
	public void checkSpecialDelimiter() {
		
		sj_cal test = new sj_cal();
        
        assertEquals(5,test.Add("//;\n2;3"));

	}
	
	@Test(expected=IllegalArgumentException.class)
	public void checkForNegativeNumberThrowException() {
		
		sj_cal test = new sj_cal();
        
        assertEquals(5,test.Add("//;\n-2;3"));

	}
	
	

}
